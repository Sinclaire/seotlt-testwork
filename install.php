<?php
require_once __DIR__ . '/src/Core/DbConnector.php';

print "DB host (leave empty, if using localhost):\n";
$host = trim(fgets(STDIN));
$host = $host ? $host : 'localhost';

print "DB user (leave empty, if using root):\n";
$user = trim(fgets(STDIN));
$user = $user ? $user : 'root';

print "DB pass:\n";
$pass = trim(fgets(STDIN));

print "DB name:\n";
$db = trim(fgets(STDIN));

$conf = [
        'host' => $host,
        'user' => $user,
        'pass' => $pass,
        'db' => $db,
    ];

file_put_contents(__DIR__ . '/src/Core/dbconfig.php', '<?php' . "\n");
file_put_contents(__DIR__ . '/src/Core/dbconfig.php', 'return ' . var_export($conf, true) . ';', FILE_APPEND);

$sql = 'CREATE TABLE IF NOT EXISTS branches (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(256),
        department_id INT,
        lat FLOAT(6,4),
        lon FLOAT(6,4)
    )';
    
$con = App\Core\DbConnector::getConnect();
$pst = $con->prepare($sql);
$pst->execute();