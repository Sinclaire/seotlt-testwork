document.addEventListener("DOMContentLoaded", () => {
    let myMap;
    ymaps.ready(() => {
        myMap = new ymaps.Map("map", {
            center: [55.76, 37.64],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        });
    });
        
    let addForm = document.querySelector('#add-form');
    addForm.addEventListener('submit', e => {
        e.preventDefault();
        e.stopPropagation();
        
        // get form data
        let formData = new FormData(addForm);
        
        let data = {
            name: formData.get('name'),
            department: formData.get('department'),
            coords: {
                lat: formData.get('lat'),
                long: formData.get('long'),
            }
        };
        
        let dataJson = JSON.stringify(data);
        
        // send form data
        fetch(new Request('/branch/add', {method: 'POST', body: dataJson}));
    });
    
    let getForm = document.querySelector('#get-form');
    getForm.addEventListener('submit', e => {
        e.preventDefault();
        e.stopPropagation();
        
        // get form data
        let formData = new FormData(getForm);
        
        let data = {
                department: formData.get('department'),
            },
            dataJson = JSON.stringify(data);
        
        // send form data
        fetch(new Request('/branch', {method: 'POST', body: dataJson}))
            .then(res => res.json())
            .then(json => {
                myMap.geoObjects.removeAll();
                
                json.map(branch => {
                    myMap.geoObjects
                    .add(
                        new ymaps.Placemark(
                            [branch.coords.lat, branch.coords.long],
                            { iconCaption: branch.name },
                            { preset: 'islands#greenDotIconWithCaption' }
                        )
                    );
                });
            });
    });
});