<?php
namespace App\Core;

use PDO;

class DbConnector
{
    public static function getConnect()
    {
        $conf = require_once __DIR__ . '/dbconfig.php';
        
        return new PDO(
            'mysql:host='.$conf['name'].';dbname='.$conf['db'].';charset=UTF8', 
            $conf['user'],
            $conf['pass']
        );
    }
}