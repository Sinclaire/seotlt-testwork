<?php
/*
* Данный код был взять из статьи "Реализация MVC патерна" на habrahabr
* https://habrahabr.ru/post/150267/
* и была измененна на более современный вид
*/
namespace App\Core;

class Route
{
	public static function Start()
	{
		// контроллер и действие по умолчанию
		$controller_name = 'Index';
		$action_name = 'index';
		$controller_namespace = 'App\\Controllers\\';
		
		$routes = explode('/', $_SERVER['REQUEST_URI']);

		// получаем имя контроллера
		if ( !empty($routes[1]) )
		{	
			$controller_name = ucfirst($routes[1]);
		}
		
		// получаем имя экшена
		if ( !empty($routes[2]) )
		{
			$action_name = $routes[2];
		}

		// добавляем префиксы
		$controller_name = $controller_namespace . $controller_name . 'Controller';
		
		if (class_exists($controller_name)) {
			$controller = new $controller_name;
		} else {
			self::ErrorPage404();
		}
		
		if(method_exists($controller, $action_name)) {
			echo $controller->$action_name();
		} else {
			self::ErrorPage404();
		}
	}
	
	public static function ErrorPage404()
	{
	    // должна отдавать специальную страницу
	    // сейчас отдает просто 404 код
        http_response_code(404);
    }
}