<?php
namespace App\Controllers;

class IndexController 
{
    public function index()
    {
        return file_get_contents(__DIR__ . '/../../static/index.html');
    }
}