<?php
namespace App\Controllers;

use App\Models\Branch;

class BranchController 
{
    public function add()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $name = $data['name'];
        $dep_id = $data['department'];
        $coords = $data['coords'];
        
        $branch = new Branch($name, $dep_id, $coords);
        $branch->save();
    }
    
    public function index()
    {
        $json_data = file_get_contents('php://input');
        
        if ($json_data) {
            $data = json_decode($json_data, true);
            $dep_id = $data['department'];
            
            $branches = Branch::getByDepartmentId($dep_id);
        } else {
            $branches = Branch::getAll();
        }
        
        $branches = array_map(
            function($branch) {
                return [
                    'name' => $branch->getName(),
                    'department_id' => $branch->getDepartmentId(),
                    'coords' => $branch->getCoords(),
                ]; 
            },
            $branches
        );
        
        return json_encode($branches);
    }
}