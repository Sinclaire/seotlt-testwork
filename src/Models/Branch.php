<?php
namespace App\Models;

use App\Core\DbConnector;

class Branch
{
    private $name = '';
    private $dep_id = 0;
    private $coords = ['lat' => 0, 'long' => 0];
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getDepartmentId()
    {
        return $this->dep_id;
    }
    
    public function getCoords()
    {
        return $this->coords;
    }
    
    public function __construct($name, $dep_id, $coords)
    {
        $this->name = $name;
        $this->dep_id = $dep_id;
        $this->coords = $coords;
    }
    
    // В данный момент save() делает только insert.
    // В реальности, метод должен либо обновлять запись, либо создавать её
    public function save()
    {
        $sql = 'INSERT INTO branches(name, department_id, lat, lon)
        VALUES (:name, :dep_id, :lat, :long)';
        
        $con = DbConnector::getConnect();
        $pst = $con->prepare($sql);
        $pst->execute([
           ':name' => $this->name, 
           ':dep_id' => $this->dep_id, 
           ':lat' => $this->coords['lat'], 
           ':long' => $this->coords['long'], 
        ]);
    }
    
    public static function getAll()
    {
        $sql = 'SELECT
                id,
                name,
                department_id,
                lat,
                lon
            FROM
                branches';
        $con = DbConnector::getConnect();
        $pst = $con->prepare($sql);
        $pst->execute();
        
        $branches = [];
        foreach ($pst as $branch) {
            $branches[] = new Branch(
                $branch['name'], 
                $branch['department_id'],
                [
                    'lat' => $branch['lat'],
                    'long' => $branch['lon'],
                ]
            );
        }
        
        return $branches;
    }
    
    public static function getByDepartmentId($dep_id)
    {
        $sql = 'SELECT
                id,
                name,
                department_id,
                lat,
                lon
            FROM
                branches
            WHERE
                department_id = :dep_id';
        
        $con = DbConnector::getConnect();
        $pst = $con->prepare($sql);
        $pst->execute([
           ':dep_id' => $dep_id,
        ]);
        
        $branches = [];
        foreach ($pst as $branch) {
            $branches[] = new Branch(
                $branch['name'], 
                $branch['department_id'],
                [
                    'lat' => $branch['lat'],
                    'long' => $branch['lon'],
                ]
            );
        }
        
        return $branches;
    }
}