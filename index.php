<?php
spl_autoload_register(function($class) {
    $class_path = str_replace('\\', '/', $class);
    $class_path = str_replace('App', 'src', $class_path);
    $class_file = $class_path . '.php';
    
    if (file_exists($class_file)) {
        require_once $class_file;
    } else {
        App\Core\Route::ErrorPage404();
    }
});

App\Core\Route::Start();